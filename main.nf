#!/usr/bin/env nextflow

nextflow.enable.dsl=1

chowlkDiagramsGlob = params.sourceDir + '/*.{xml,drawio}'
sourceOntologiesGlob = params.sourceDir + '/*.{ttl,nt,n3,nq,trig,rdf,owl,jsonld,trdf,rt,rj,trix}'
shaclShapesGlob = '/' + params.shaclDir + '/*.{ttl,xml,nt,n3,jsonld}'
templatesGlob = '/' + params.templateDir + '/*.html'

serializations = Channel.of(['TURTLE', 'ttl'], ['JSON-LD', 'jsonld'])
chowlkDiagrams = Channel
    .fromPath(chowlkDiagramsGlob)
    .multiMap { it ->
        toTtl: it
        toSvg: it
    }
sourceOntologies = Channel.fromPath(sourceOntologiesGlob)
shaclShapes = Channel.fromPath([
    projectDir + shaclShapesGlob,
    params.sourceDir + shaclShapesGlob
])
templates = Channel.fromPath([projectDir + templatesGlob])

process convertChowlkDiaToTurtle {

    container params.images.registry + 'chowlk:' + params.images.tag

    input:
    path chowlkDiagram from chowlkDiagrams.toTtl

    output:
    path '*.ttl' into chowlkOntologies

    """
    converter $chowlkDiagram ${chowlkDiagram.getBaseName()}.ttl --type ontology --format ttl
    """

}

process mergeOntologies {

    container 'stain/jena:4.0.0'

    input:
    path '*' from chowlkOntologies.concat(sourceOntologies).collect()

    output:
    path 'ontology-merged.ttl' into ontology


    """
    riot --quiet --formatted=Turtle * > ontology-merged.ttl
    """

}

ontology = ontology.multiMap { it ->
        priorVersion: it
        diff: it
        documentation: it
        rdfSerialization: it
        validation: it
}

process createOntologySerializations {

    container 'stain/jena:4.0.0'

    publishDir params.outputDir, mode: 'copy'

    input:
    tuple val(format), val(suffix) from serializations
    path ontology from ontology.rdfSerialization

    output:
    path 'index.*'


    """
    riot --quiet --formatted=$format $ontology > index.$suffix
    """

}

process createDocumentation {

    container params.images.registry + 'pylode:' + params.images.tag

    publishDir params.outputDir, mode: 'copy'

    input:
    path 'index.ttl' from ontology.documentation

    output:
    path 'index.html'

    """
    python -m pylode index.ttl -o index.html
    """

}

process mergeShaclShapes {

    container 'stain/jena:4.0.0'

    publishDir params.outputDir, mode: 'copy'

    input: 
    path 'shacl-*.ttl' from shaclShapes.collect()

    output:
    path 'merged-shacl-shapes.ttl' into mergedShaclShapes

    """
    riot --quiet --formatted=Turtle shacl-*.ttl > merged-shacl-shapes.ttl
    """

}

process validateOntology {

    container params.images.registry + 'pyshacl:' + params.images.tag

    publishDir params.outputDir, mode: 'copy'

    input:
    path ontology from ontology.validation
    path 'merged-shacl-shapes.ttl' from mergedShaclShapes

    output:
    path 'validation-report.ttl' into validationResult

    """
    trap 'if [[ \$? == 1 ]]; then echo OK; exit 0; fi' EXIT
    pyshacl --allow-warnings -s merged-shacl-shapes.ttl -f turtle $ontology > validation-report.ttl
    """

}

process renderReportAndShapes {

    container params.images.registry + 'jekyll-rdf:' + params.images.tag

    publishDir params.outputDir, mode: 'copy'

    input:
    path 'validation-report.ttl' from validationResult
    path 'merged-shacl-shapes.ttl' from mergedShaclShapes
    path template from templates

    output:
    path '*-*.html'

    """
    render-shacl $template validation-report.ttl merged-shacl-shapes.ttl 
    """

}


process convertChowlkDiaToSvg {

    container params.images.registry + 'drawio:' + params.images.tag

    publishDir params.outputDir, mode: 'copy'

    input:
    path chowlkDiagram from chowlkDiagrams.toSvg

    output:
    path '*.svg'

    """
    drawio --export -f svg $chowlkDiagram
    """

}

process getPriorVersionFromOntology {

    container params.images.registry + 'sparql-integrate:' + params.images.tag

    input:
    path ontology from ontology.priorVersion

    output:
    stdout priorVersionUriJson

    """
    #!/usr/bin/env -S rpt integrate --jq $ontology

    prefix owl:   <http://www.w3.org/2002/07/owl#>

    SELECT ?version WHERE {

      ?ontology a owl:Ontology ;
        owl:priorVersion ?version .
    }
    """

}

process getPriorVersionFromJson {

    container params.images.registry + 'sparql-integrate:' + params.images.tag

    input:
    stdin priorVersionUriJson

    output:
    stdout priorVersion

    """
    jq -r '.[0].version.id'
    """

}

process diffOntology {

    container params.images.registry + 'bubastis:' + params.images.tag

    publishDir params.outputDir, mode: 'copy'

    errorStrategy 'ignore'

    input:
    path newOntology from ontology.diff
    val oldOntology from priorVersion
                                    .filter { !it.startsWith("null") }
                                    .map{ it.trim() }

    output:
    path diff

    """
    bubastis -ontology1 $newOntology -ontology2 $oldOntology -output diff
    """

}
