# Local development

# This uses local container
build-test-ontology-develop: clean build-images
	nextflow run -c develop.config main.nf

# This uses containers from the GitLab registry with the `main` tag
build-test-ontology-main: clean
	nextflow run main.nf --sourceDir test/input/

test: build-test-ontology-main
	diff -u public test/output/public

clean:
	nextflow clean -f || true
	rm -fr \
		ressources/flowchart.png \
		./public

images = $(notdir $(wildcard ./images/*))
build-image = docker build --tag $(image):develop ./images/$(image)
build-images:
	$(foreach image, $(images), $(build-image);)

# GitLab CI Jobs

# CI_COMMIT_TAG is only set, when a branch with tag is built
CI_COMMIT_TAG ?= $(CI_COMMIT_BRANCH)

# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-caching-example
# https://stackoverflow.com/questions/6519234/cant-assign-variable-inside-recipe
define gitlab-ci-build-push-image =
	$(eval base_tag=$(CI_REGISTRY_IMAGE)/$(image))
	docker pull $(base_tag):$(CI_COMMIT_BRANCH) || true
	docker build \
		--cache-from $(base_tag):$(CI_COMMIT_BRANCH) \
		--tag $(base_tag):$(CI_COMMIT_TAG) \
		--tag $(base_tag):$(CI_COMMIT_BRANCH) \
		./images/$(image)
	docker push $(base_tag):$(CI_COMMIT_TAG)
	docker push $(base_tag):$(CI_COMMIT_BRANCH)
endef

gitlab-ci-build-push-images:
	echo $(CI_REGISTRY_PASSWORD) | docker login $(CI_REGISTRY) --username $(CI_REGISTRY_USER) --password-stdin
	$(foreach image, $(images), $(gitlab-ci-build-push-image);)

gitlab-ci-test:
	nextflow run main.nf --sourceDir test/input/ --images.tag $(CI_COMMIT_TAG)

.PHONY: build-test-ontology-develop build-test-ontology-main test clean build-images gitlab-ci-build-push-images gitlab-ci-test
